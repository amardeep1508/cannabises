#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from math import sqrt

from sklearn.model_selection import LeaveOneOut
from sklearn.metrics import mean_squared_error, r2_score

from mlxtend.regressor import StackingRegressor
from sklearn.svm import SVR

# from sklearn.linear_model import Ridge
# from sklearn.linear_model import Lasso
# from sklearn.linear_model import LinearRegression
# from sklearn.model_selection import GridSearchCV
# import warnings

# %%

# read phenotype features file for trial 1 cannabises
# filtered_name is the file which contains FFW and phenotype features
plant_xls = pd.read_csv('filtered_name.csv')
plant_xls.head()

# filter kush and angel cultivate
Kush_xls = plant_xls[(plant_xls['MATNM'] == 'Kush')]
Angel_xls = plant_xls[(plant_xls['MATNM'] == 'Angel')]

# Remove null feature columns from angel and kyush
null_cols_angel = Angel_xls.columns[Angel_xls.isnull().any()]
Angel_xls.drop(null_cols_angel, axis=1, inplace=True)

null_cols_kush = Kush_xls.columns[Kush_xls.isnull().any()]
Kush_xls.drop(null_cols_kush, axis=1, inplace=True)

# remove those columns which are not feature
Angel_xls1 = Angel_xls.iloc[:, 15:]
Kush_xls1 = Kush_xls.iloc[:, 15:]

# use features selected through exhaustive search on angel
# new_kush = Kush_xls1.loc[:,['FWFR_91', 'DCHL.L2_28', 'DCHL.L3_28', 'DCHL.L4_28']]
new_kush = Kush_xls1.loc[:, ['FWFR_91', 'INL_21', 'DANT.L1_14', 'DCHL.L1_14', 'PLTHGT_7', 'PLTHGT_14']]

# extract features and measured FFW from new_angel
X_df = new_kush.iloc[:, 1:]
y_df = new_kush.iloc[:, 0]
print(y_df[:5])
print(X_df.head())

#%% Initializing stacked regression model

svr_lin1 = SVR(kernel='linear', C=1.0, gamma='scale')
svr_lin2 = SVR(kernel='linear', C=2.0, gamma='scale')
svr_lin3 = SVR(kernel='linear', C=3.0, gamma='scale')
svr_lin4 = SVR(kernel='linear', C=4.0, gamma='scale')
svr_lin6 = SVR(kernel='linear', C=10.0, gamma='scale')

# Meta regressor
svr_rbf = SVR(kernel='rbf', C=10.0, gamma=0.1)

regressors = [svr_lin1, svr_lin2,
              svr_lin3, svr_lin4,
              svr_lin6]
# svr_lin5,
stregr = StackingRegressor(regressors=regressors,
                           meta_regressor=svr_rbf)

#%% below is leave one out cross validation (loocv)

loo = LeaveOneOut()
prediction_y = []  # list to hold prediction from loocv
true_y = []  # list to hold true values
for train_index, test_index in loo.split(X_df, y_df):
    # print("TEST:", test_index)
    X_train, X_test = X_df.iloc[train_index], X_df.iloc[test_index]
    y_train, y_test = y_df.iloc[train_index].values, y_df.iloc[test_index].values

    # fit stacked regression on training set
    stregr.fit(X_train.values, y_train)

    # predict output and append in list
    y_pred = stregr.predict(X_test.values)
    prediction_y.append(y_pred)
    true_y.append(y_test)

np_prediction_y = np.vstack(prediction_y)
np_true_y = np.vstack(true_y)

# get r2 and mse from predicted and true values
r2_score_loocv = r2_score(np_true_y, np_prediction_y)
mse_loocv = mean_squared_error(np_true_y, np_prediction_y)
print(r2_score_loocv)
print(mse_loocv)

#%% RMSE for each treatment
pl_bc1 = sqrt(mean_squared_error(np_prediction_y[0:14], np_true_y[0:14]))
print(pl_bc1)
pl_bc2 = sqrt(mean_squared_error(np_prediction_y[14:30], np_true_y[14:30]))
print(pl_bc2)
pl_control = sqrt(mean_squared_error(np_prediction_y[30:], np_true_y[30:]))
print(pl_control)

#%% plot predicted and true FFW values
with plt.style.context('ggplot'):
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.scatter(np_prediction_y[:14], np_true_y[:14], c='red', alpha=0.5, label='BC1')
    ax.scatter(np_prediction_y[14:30], np_true_y[14:30], c='blue', alpha=0.5, label='BC2')
    ax.scatter(np_prediction_y[30:], np_true_y[30:], c='green', alpha=0.5, label='CTRL')
    ax.set_xlabel('predicted FFW', fontsize=15)
    ax.set_ylabel('measured FFW', fontsize=15)
    ax.set_title('Angel---> r2_loocv: ' + str(r2_score_loocv) + ', MSE_loocv: ' + str(mse_loocv))
    ax.legend()
ax.grid(True)
fig.tight_layout()

plt.show()

#%% test with random noise matrix

svr_lin1 = SVR(kernel='linear', C=1.0, gamma='scale')
svr_lin2 = SVR(kernel='linear', C=2.0, gamma='scale')
svr_lin3 = SVR(kernel='linear', C=3.0, gamma='scale')
svr_lin4 = SVR(kernel='linear', C=4.0, gamma='scale')
svr_lin6 = SVR(kernel='linear', C=10.0, gamma='scale')

# Meta regressor
svr_rbf = SVR(kernel='rbf', C=10.0, gamma=0.1)

regressors = [svr_lin1, svr_lin2,
              svr_lin3, svr_lin4,
              svr_lin6]
# svr_lin5,
stregr1 = StackingRegressor(regressors=regressors,
                            meta_regressor=svr_rbf)

Xrand = np.random.rand(44, 5)

# Below we use random number matrix as features to predict FFW with respect to true FFW using Loocv techniquw
loo = LeaveOneOut()
predi_y = []  # list to hold prediction from loocv
all_y = []  # list to hold true values from loocv
for train_index, test_index in loo.split(Xrand, y_df):
    # print("TEST:", test_index)
    X_train, X_test = Xrand[train_index], Xrand[test_index]
    y_train, y_test = y_df.iloc[train_index].values, y_df.iloc[test_index].values

    # fit training model on random number
    stregr1.fit(X_train, y_train)

    # predict and store in lists
    y_pred = stregr1.predict(X_test)
    predi_y.append(y_pred)
    all_y.append(y_test)

y_cv = np.vstack(predi_y)
y1 = np.vstack(all_y)

score_loocv = r2_score(y1, y_cv)
mse_loocv = mean_squared_error(y1, y_cv)
print(score_loocv)
print(mse_loocv)

#%% plot noise predicted values
with plt.style.context(('ggplot')):
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.scatter(y_cv[:14], y1[:14], c='red', alpha=0.5, label='BC1')
    ax.scatter(y_cv[14:30], y1[14:30], c='blue', alpha=0.5, label='BC2')
    ax.scatter(y_cv[30:], y1[30:], c='green', alpha=0.5, label='CTRL')
    # ax.plot(z[1]+z[0]*y1, y1, c='blue', linewidth=1)
    ax.set_xlabel('predicted FFW', fontsize=15)
    ax.set_ylabel('measured FFW', fontsize=15)
    ax.set_title('Angel---> r2_loocv: ' + str(score_loocv) + ', MSE_loocv: ' + str(mse_loocv))
    ax.legend()

ax.grid(True)
fig.tight_layout()

plt.show()

#%% extract labels for plants

labels = Kush_xls['TRT'].values
labels = np.reshape(labels, (len(labels), 1))

#%% store true values and prediction from features and noise in result

result = np.concatenate([labels, np_true_y, y_cv, np_prediction_y], axis=1)
result = pd.DataFrame(data=result, columns=['TRT', 'ACT', 'noise_PRED', 'Feature_pred'])
result.head()

#%% smarm and box plot of true and predicted values
f, axes = plt.subplots(3, 1, figsize=(15, 8))
sns.swarmplot(x=result["ACT"], color='red', ax=axes[0])
sns.boxplot(x=result["ACT"], ax=axes[0])

sns.swarmplot(x=result["Feature_pred"], color='red', ax=axes[1])
sns.boxplot(x=result["Feature_pred"], ax=axes[1])

sns.swarmplot(x=result["noise_PRED"], color='red', ax=axes[2])
sns.boxplot(x=result["noise_PRED"], ax=axes[2])
plt.show()
#%% smarm and box plot of true and predicted values for each treatment
fig1, axes = plt.subplots(3, 1, figsize=(10, 8))
sns.swarmplot(x="TRT", y="ACT", color='red', data=result, ax=axes[0])
sns.boxplot(x="TRT", y="ACT", data=result, ax=axes[0])

sns.swarmplot(x="TRT", y="Feature_pred", color='red', data=result, ax=axes[1])
sns.boxplot(x="TRT", y="Feature_pred", data=result, ax=axes[1])

sns.swarmplot(x="TRT", y="noise_PRED", color='red', data=result, ax=axes[2])
sns.boxplot(x="TRT", y="noise_PRED", data=result, ax=axes[2])
plt.show()
#%% plot data distribution of true and predicted values

fig2, axes = plt.subplots(1, 3, figsize=(15, 5))
sns.distplot(result["ACT"], ax=axes[0])
sns.distplot(result["Feature_pred"], ax=axes[1])
sns.distplot(result["noise_PRED"], ax=axes[2])
plt.show()
