import pandas as pd
import numpy as np


def combine_nir(list_nir_raw_plants, axis_=0):
    """

        This function combines dataframes of nir data

        Args:
            list_nir_raw_plants (pandas.DataFrame): list of dataframes
            axis_ (int): give axis to combine. default row wise

        Returns:
            filtered_nir_plants (pandas.DataFrame): Filtered DataFrame

        >>> combine_nir([nir_raw_plants1, nir_raw_plants2], 1)

    """

    combine_nir_plants = pd.concat(list_nir_raw_plants, axis=axis_)

    return combine_nir_plants


def filter_nir(nir_raw_plants, start, end):
    """

        This function is used to cut head and tail of NIR data
        to eliminate temperature effect.

        Args:
            nir_raw_plants (pandas.DataFrame): Input raw nir DataFrame
            start (int): starting point of NIR sample
            end (int): ending point of NIR sample

        Returns:
            filtered_nir_plants (pandas.DataFrame): Filtered DataFrame

        >>> filter_nir(nir_raw_plants,20,200)

    """
    plant_info = nir_raw_plants.iloc[:, :4]
    plant_nir = nir_raw_plants.iloc[:, 4:]
    plant_nir = plant_nir.iloc[:, start:end]
    filtered_nir_plants = pd.concat([plant_info, plant_nir], axis=1)

    return filtered_nir_plants


def snv_nir(nir_raw_plants):
    """

        Perform Standard Normal Variate (SNV) scatter correction

        Args:
            nir_raw_plants (pandas.DataFrame): DataFrame of raw NIR samples

        Returns:
            nir_snv_plants (pandas.DataFrame): snv corrected DataFrame of NIR samples

        >>> snv_nir(nir_raw_plants)

    """
    plant_info = nir_raw_plants.iloc[:, :4]
    plant_nir = nir_raw_plants.iloc[:, 4:]

    for i in range(plant_nir.shape[0]):
        temp = plant_nir.iloc[i, :]
        temp = (temp - np.mean(temp)) / np.std(temp)
        plant_nir.iloc[i, :] = temp

    nir_snv_plants = pd.concat([plant_info, plant_nir], axis=1)

    return nir_snv_plants


def msc_nir(nir_raw_plants, reference=None):
    """

        Perform Multiplicative scatter correction

        Args:
            nir_raw_plants (pandas.DataFrame): DataFrame of raw NIR samples
            reference (numpy.array): reference if available otherwise None

        Returns:
            nir_msc_plants (pandas.DataFrame): msc corrected DataFrame of NIR samples

        >>> msc_nir(nir_raw_plants)

    """

    plant_info = nir_raw_plants.iloc[:, :4]
    plant_nir = nir_raw_plants.iloc[:, 4:]
    input_data = plant_nir.values
    nir_plants_wls = plant_nir.columns

    # mean centre correction
    for i in range(input_data.shape[0]):
        input_data[i, :] -= input_data[i, :].mean()
    # Get the reference spectrum. If not given, estimate it from the mean
    if reference is None:
        # Calculate mean
        ref = np.mean(input_data, axis=0)
    else:
        ref = reference
    # Define a new array and populate it with the corrected data
    data_msc = np.zeros_like(input_data)
    for i in range(input_data.shape[0]):
        # Run regression
        fit = np.polyfit(ref, input_data[i, :], 1, full=True)
        # Apply correction
        data_msc[i, :] = (input_data[i, :] - fit[0][1]) / fit[0][0]

    plant_nir = pd.DataFrame(data_msc, columns=nir_plants_wls)
    nir_msc_plants = pd.concat([plant_info, plant_nir], axis=1)

    return nir_msc_plants, ref
