import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib._color_data as mcd
from matplotlib.lines import Line2D

def plot_std_mean(bins=None, **kwargs):
    """

        This function display mean and std deviantion of NIR data

        Args:
            bins (int): Input bins to be display on axes. default 'None'
            **kwargs (dict): kwargs is a dictionary of pandas dataframe.
            This dictionary helps in making legend on axis

        Returns:
            axes (matplotlib.axes): The returned matplotlib axes class

        >>> plot_std_mean(10,**{'bc1_angel':bc1_angel_df,'bc2_angel':bc2_angel_df})

    """
    fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 8), tight_layout=True)

    for key, value in kwargs.items():
        plant_nir = value.iloc[:, 4:]
        ax1.plot(plant_nir.columns, plant_nir.mean(axis=0).values, '-o', alpha=0.5, label=key)
        ax2.plot(plant_nir.columns, plant_nir.std(axis=0).values, '-o', alpha=0.5, label=key)
    ax1.legend()
    ax2.legend()
    ax1.set_xlabel("wavelength locations")
    ax1.set_ylabel("mean absorption (log(1/(i/i_ref)))")
    ax2.set_xlabel("wavelength locations")
    ax2.set_ylabel("std dev")
    ax1.locator_params(axis='x', nbins=bins)
    ax1.locator_params(axis='y', nbins=bins)
    ax2.locator_params(axis='x', nbins=bins)
    ax2.locator_params(axis='y', nbins=bins)
    save_fig_title = "plot_std_mean.png"
    fig.savefig(save_fig_title)
    return ax1, ax2


def plot_temperature(bins=None, **kwargs):
    """

            This function display temperature of NIR data

            Args:
                bins (int): Input bins to be display on axes. default 'None'
                **kwargs (dict): kwargs is a dictionary of pandas dataframe.
                This dictionary helps in making legend on axis

            Returns:
                axes (matplotlib.axes): returns matplotlib axis class

            >>> plot_temperature(10,{'bc1_angel':bc1_angel_df,'bc2_angel':bc2_angel_df})

    """

    fig, ax1 = plt.subplots(1, figsize=(10, 8), tight_layout=True)

    # df is a tuple with arguments
    for key, value in kwargs.items():
        ax1.plot(value['Plant_temperature'], '-o', alpha=0.5, label=key)
    ax1.legend()
    ax1.set_xlabel("samples");
    ax1.set_ylabel("temperature")
    ax1.locator_params(axis='y', nbins=bins)
    save_fig_title = "plot_temperature.png"
    fig.savefig(save_fig_title)
    return ax1


def plot_time_temperature(bins=None, **kwargs):
    """

            This function display temperature of NIR data

            Args:
                bins (int): Input bins to be display on axes. default 'None'
                **kwargs (dict): kwargs is a dictionary of pandas dataframe.
                This dictionary helps in making legend on axis

            Returns:
                axes (matplotlib.axes): returns matplotlib axis class

            >>> plot_time_temperature(10,{'bc1_angel':bc1_angel_df,'bc2_angel':bc2_angel_df})

    """

    fig, ax1 = plt.subplots(1, figsize=(10, 8), tight_layout=True)

    # df is a tuple with arguments
    for key, value in kwargs.items():
        ax1.plot(value['Plant_temperature'], '-o', alpha=0.5, label=key)
    ax1.legend()
    ax1.set_xlabel("samples");
    ax1.set_ylabel("temperature")
    ax1.locator_params(axis='y', nbins=bins)
    save_fig_title = "plot_temperature.png"
    fig.savefig(save_fig_title)
    return ax1


def plot_outlier(**kwargs):
    """

        This function display nir data under mean (+/-) 3 std dev. restriction. This help
        in identifying nir samples which dont fall in this limits

        Args:
           **kwargs (dict): kwargs is a dictionary of pandas.Dataframe.
            This dictionary helps in making legend on axis

        Returns:
            axes (matplotlib.axes): The returned matplotlib axes class

        >>> plot_outlier(**{'bc1_angel':bc1_angel_df,'bc2_angel':bc2_angel_df})

    """
    fig, ax = plt.subplots(1, figsize=(15, 10))
    items_no = len(kwargs.items())
    items_label = []

    colors_ = list(colors._colors_full_map.values())
    colors_list = []

    for i, (key, value) in enumerate(kwargs.items()):
        plant_nir = value.iloc[:, 4:]
        wavelengths = list(map(int, plant_nir.columns))
        max_ = key + "mean + 3 std_dev"
        min_ = key + "mean - 3 std_dev"

        items_label.append(key)
        items_label.append(max_)
        items_label.append(min_)
        colors_list.append(colors_[i])
        colors_list.append(colors_[i + items_no])
        colors_list.append(colors_[i + items_no])

        max_plant = plant_nir.T.values.mean(axis=1) + (3 * plant_nir.T.values.std(axis=1))
        min_plant = plant_nir.T.values.mean(axis=1) - (3 * plant_nir.T.values.std(axis=1))

        ax.plot(wavelengths, plant_nir.T.values, color=colors_[i],
                linestyle='-.', alpha=0.6, label=key)
        ax.plot(wavelengths, max_plant, color=colors_[i + items_no],
                linewidth=4, alpha=0.5, label=key + str(max))
        ax.plot(wavelengths, min_plant, color=colors_[i + items_no],
                linewidth=4, alpha=0.5, label=key + str(min))
        ax.plot(wavelengths, plant_nir.T.values.mean(axis=1), linewidth=2,
                color=colors_[i], alpha=0.7)

    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='--') for c in colors_list]
    ax.legend(lines, items_label)
    ax.grid(True)
    return ax


def plot_rawdata(**kwargs):
    """

        This function display raw nir data

        Args:
           **kwargs (dict): kwargs is a dictionary of pandas.Dataframe.
            This dictionary helps in making legend on axis

        Returns:
            axes (matplotlib.axes): The returned matplotlib axes class

        >>> plot_rawdata(**{'bc1_angel':bc1_angel_df,'bc2_angel':bc2_angel_df})

    """
    fig, ax = plt.subplots(1, figsize=(15, 10))
    items_no = len(kwargs.items())
    items_label = []

    colors_ = list(mcd.XKCD_COLORS.values())  # list(colors._colors_full_map.values())
    colors_list = []

    for i, (key, value) in enumerate(kwargs.items()):
        plant_nir = value.iloc[:, 4:]
        wavelengths = list(map(int, plant_nir.columns))
        items_label.append(key)
        colors_list.append(colors_[i])

        ax.plot(wavelengths, plant_nir.T.values, color=colors_[i],
                linestyle='-.', alpha=0.7, label=key)
        ax.plot(wavelengths, plant_nir.T.values.mean(axis=1), linewidth=4,
                color=colors_[i], alpha=0.8)

    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='--') for c in colors_list]
    ax.legend(lines, items_label)
    ax.grid(True)
    return ax