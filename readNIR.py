import os
import fnmatch
import pandas as pd
import numpy as np


def read_canabies_trial1(path, trt, cultivate, plantIDs, leafID):
    """

        Read canabies plants NIR data belongs to trial 1

        Args:
            path (str): folder location in drive
            trt (str): treatment e.g. 'A' for 'BC1'
            cultivate (str): canabies cultivar e.g. 'Kush'
            plantIDs (list): list of plants e.g. ['A1','A4','D1']
            leafID (str): leaf Id  e.g. 'l1'


        Returns:
            nir_raw_plants (DataFrame): DataFrame contains plant_id, nir_sample time,
             nir_sample_temperature and  raw_nir sample

        >>> read_canabies_trial1('/home/deep','A','Angel',['A1','B3','C4'],'L1')


    """

    os.chdir(path)
    ls_files = os.listdir()  # list of content in the folder
    nir_raw_plants = pd.DataFrame()

    for plantID in plantIDs:
        # pattern to filter plant from list
        filter_condition = trt + "*" + cultivate + "*" + plantID + "*" + leafID + "*" + '.csv'
        filtered_plant_csv = fnmatch.filter(ls_files, filter_condition)

        # read raw nir sample and wavelengths
        plant_sample_nir = pd.read_csv(filtered_plant_csv[0], skiprows=29)['Absorbance (AU)'].values
        plant_sample_wavelengths = pd.read_csv(filtered_plant_csv[0], skiprows=29)['Wavelength (nm)'].values
        plant_sample_nir = np.reshape(plant_sample_nir, [1, len(plant_sample_nir)])
        plant_sample_wavelengths = list(map(int, plant_sample_wavelengths))
        plant_sample_wavelengths = list(map(str, plant_sample_wavelengths))

        # read other important info of the nir sample
        plant_sample = pd.read_csv(filtered_plant_csv[0])
        plant_sample_temperature = float(plant_sample.index.values[10][1])
        plant_sample_time = plant_sample.index.values[13][1]
        plant_sample_time = "".join(plant_sample_time.split('@'))
        plant_sample_time = pd.to_datetime(plant_sample_time)

        # create respective data_frames. Finally, merge all data together
        plant_info = {'PlantID': plantID, 'TRT': trt,
                      'Plant_temperature': plant_sample_temperature,
                      'Plant_time': plant_sample_time}
        plant_info = pd.DataFrame(plant_info, index=[0])
        plant_nir = pd.DataFrame(data=plant_sample_nir, columns=plant_sample_wavelengths)
        plant_pd = pd.concat([plant_info, plant_nir], axis=1)
        nir_raw_plants = nir_raw_plants.append(plant_pd, ignore_index=True)

    return nir_raw_plants


def read_canabies_trial4(path, tray, plantIDs):
    """

        Read canabies plants NIR data belongs to trial 4
        Args:
            path (str): folder location in drive
            tray (str): treatment e.g. 'A' for 'BC1'
            plantIDs (list): list of plants e.g. ['A1','A4','D1']



        Returns:
            nir_raw_plants (DataFrame): DataFrame contains plant_id, nir_sample time,
             nir_sample_temperature and  raw_nir sample

        >>> read_canabies_trial4('/home/deep','A1',['IV_13','IV_32'])

    """

    os.chdir(path)
    ls_files = os.listdir()  # list of content in the folder
    nir_raw_plants = pd.DataFrame()

    for plantID in plantIDs:
        # pattern to filter plant from list
        filter_condition = tray + "*" + plantID + "2020" + '*' + '.csv'
        filtered_plant_csv = fnmatch.filter(ls_files, filter_condition)

        # read raw nir sample and wavelengths
        plant_sample_nir = pd.read_csv(filtered_plant_csv[0], skiprows=29)['Absorbance (AU)'].values
        plant_sample_wavelengths = pd.read_csv(filtered_plant_csv[0], skiprows=29)['Wavelength (nm)'].values
        plant_sample_nir = np.reshape(plant_sample_nir, [1, len(plant_sample_nir)])
        plant_sample_wavelengths = list(map(int, plant_sample_wavelengths))
        plant_sample_wavelengths = list(map(str, plant_sample_wavelengths))

        # read other important info of the nir sample
        plant_sample = pd.read_csv(filtered_plant_csv[0])
        plant_sample_temperature = float(plant_sample.index.values[10][1])
        plant_sample_time = plant_sample.index.values[13][1]
        plant_sample_time = "".join(plant_sample_time.split('@'))
        plant_sample_time = pd.to_datetime(plant_sample_time)

        # create respective data_frames. Finally, merge all data together
        plant_info = {'PlantID': plantID, 'TRT': tray,
                      'Plant_temperature': plant_sample_temperature,
                      'Plant_time': plant_sample_time}
        plant_info = pd.DataFrame(plant_info, index=[0])
        plant_nir = pd.DataFrame(data=plant_sample_nir, columns=plant_sample_wavelengths)
        plant_pd = pd.concat([plant_info, plant_nir], axis=1)
        nir_raw_plants = nir_raw_plants.append(plant_pd, ignore_index=True)

    return nir_raw_plants
